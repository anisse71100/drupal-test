<?php

namespace Drupal\getservice\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines GetServiceController class.
 */
class GetServiceController extends ControllerBase
{


  /**
   * 
   */
  public function error_page()
  {
    return [
      '#theme' => 'error-page',
    ];
  }


  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function getservice_page(){

    $data = array();
    $res = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'service']);
    $employe_titles = array();

  

    foreach ($res as $service) {

        $entityref = $service->field_relation_employe;


        foreach ($entityref->referencedEntities() as $employe) {
            $data[] = [
                'nom'=> $employe->getTitle(),
                'prenom' => $employe->field_prenom->value,
                'adresse' => $employe->field_adresse->value

            ];
        }
    

        $data[] = [
            'title'=> $service->title->value,
            'description'=> $service->body->value,
            'image' => $service->field_imageservice->entity->getFileUri()
        ];


    }
    $final = array();
    $final['employes'] = $data;

    return [
        '#theme' => 'listing',
        '#data' => $final,
      ];
}


public function getserviceREST(){

    $data = array();
    $res = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'service']);

    foreach ($res as $service) {

        $entityref = $service->field_relation_employe;


        foreach ($entityref->referencedEntities() as $employe) {
            $data[] = $employe->getTitle();
            $data[] = $employe->field_prenom->value;
            $data[] = $employe->field_adresse->value;
        }

        $data[] = $service->title->value;
        $data[] = $service->body->value;
        $data[] = $service->field_imageservice->entity->getFileUri();

    }
    return new JsonResponse($data);
}

}
